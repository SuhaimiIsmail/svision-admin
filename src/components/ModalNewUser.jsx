import { useState } from "react";
import axios from "axios";

export default function ModalNewUser({ open, onClose }) {
  
  const [data, setData] = useState({
		fullname: "",
		email: "",
		password: "",
    role: "user",
		active: "true",
	});
	const [error, setError] = useState("");

	const handleChange = ({ currentTarget: input }) => {
		setData({ ...data, [input.name]: input.value });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			const url = "http://13.251.155.186:8001/api/register";
			const { data: res } = await axios.post(url, data);
			console.log(res.message);
      window.location.reload()
		} catch (error) {
			if (
				error.response &&
				error.response.status >= 400 &&
				error.response.status <= 500
			) {
				setError(error.response.data.message);
			}
		}
	};

  if (!open) return null;

  return (
    <div className="fixed h-[100%] w-[100%]">
      <div className="fixed top-[50%] left-[50%] flex translate-x-[-50%] translate-y-[-50%]">
        <div className="w-full max-w-xl rounded-sm bg-blue-gray-900 outline outline-gray-400">
          <p
            className="ml-[28.5rem] mt-4 flex h-7 w-7 cursor-pointer items-center justify-center rounded-full bg-red-400 text-white"
            onClick={onClose}
          >
            X
          </p>
          <form className="rounded bg-blue-gray-900 px-8 pt-6 pb-8 text-white shadow-md" onSubmit={handleSubmit}>
            <div className="flex flex-row gap-5">
              <div className="flex flex-col">
                <div className="mb-4">
                  <label
                    className="mb-2 block text-sm font-bold"
                  >
                    Full Name
                  </label>
                  <input
                    className="focus:shadow-outline w-full appearance-none rounded border py-2 px-3 leading-tight text-gray-700 shadow focus:outline-none"
                    name="fullname"
                    type="text"
                    placeholder="Full name"
                    value={data.fullname}
                    onChange={handleChange}
                  />
                </div>
                <div className="mb-4">
                  <label className="mb-2 block text-sm font-bold">
                    Email
                  </label>
                  <input
                    className="focus:shadow-outline w-full appearance-none rounded border py-2 px-3 leading-tight text-gray-700 shadow focus:outline-none"
                    name="email"
                    type="email"
                    placeholder="Email"
                    value={data.email}
                    onChange={handleChange}
                  />
                </div>
                <div className="mb-6">
                  <label
                    className="mb-2 block text-sm font-bold"
                  >
                    Password
                  </label>
                  <input
                    className="focus:shadow-outline mb-3 w-full appearance-none rounded border py-2 px-3 leading-tight text-gray-700 shadow focus:outline-none"
                    name="password"
                    type="password"
                    placeholder="Password"
                    value={data.password}
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="flex flex-col">
                <div className="mb-4">
                  <label className="mb-2 block text-sm font-bold">
                    Role
                  </label>
                  <select
                    name="role"
                    value={data.role}
                    onChange={handleChange}
                    className="focus:shadow-outline w-52 appearance-none rounded border py-2 px-3 leading-tight text-gray-600 shadow focus:outline-none"
                  >
                    <option value="user">User</option>
                    <option value="admin">Admin</option>
                  </select>
                </div>
                <div className="mb-4">
                  <label className="mb-2 block text-sm font-bold">
                    Active
                  </label>
                  <select
                    name="active"
                    value={data.active}
                    onChange={handleChange}
                    className="focus:shadow-outline w-52 appearance-none rounded border py-2 px-3 leading-tight text-gray-600 shadow focus:outline-none"
                  >
                    <option value="true">Active</option>
                    <option value="false">Inactive</option>
                  </select>
                </div>
              </div>
            </div>
            {error && <div>{error}</div>}
            <div className="flex items-center">
              <button
                className="focus:shadow-outline rounded bg-blue-500 py-2 px-4 font-bold text-white hover:bg-blue-700 focus:outline-none"
                type="submit"
              >
                Sign Up
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
