import { useEffect, useState } from "react";
import { CgTrash } from "react-icons/cg";
import { BsPlus } from "react-icons/bs";
import moment from "moment";
import axios from "axios";

import {
  Card,
  CardHeader,
  CardBody,
  Typography,
  Avatar,
  Chip,
} from "@material-tailwind/react";
import ModalNewUser from "@/components/ModalNewUser";

export function Tables() {
  const [openModal, setOpenModal] = useState(false);
  const [userlist, setUserlist] = useState([])

  useEffect(() => {
		axios.get("http://13.251.155.186:8001/api/readAllUser").then((response) => {
			setUserlist(response.data)
		})
	}, )

  const deleteUser = (id, fullname) => {
    if(confirm(`Are you confirm to delete the ${fullname} data?`)){
      axios.delete(`http://13.251.155.186:8001/api/delete/${id}`).then(
        window.location.reload()
      )
    }
    }

  const updateUser = (id, active) => {
		if(active === 'true'){
			const newStatus = 'false'
			axios.put("http://13.251.155.186:8001/api/update", {
			id: id,
			active: newStatus
			}).then(
				// window.location.reload()
        console.log("Success")
			)
		}
		else {
			const newStatus = 'true'
			axios.put("http://13.251.155.186:8001/api/update", {
			id: id,
			active: newStatus
			}).then(
				// window.location.reload()
        console.log("Success")
			)
		}
		
	}

  return (
    <div className="mt-12 mb-8 flex flex-col gap-12">
      <Card>
        <CardHeader variant="gradient" color="blue" className="mb-8 p-6">
          <Typography variant="h6" color="white">
            Users Table
          </Typography>
        </CardHeader>
        <button onClick={() => setOpenModal(true)} className="my-3 ml-[63rem] flex w-28 flex-row items-center bg-green-600 py-1 px-2 text-white outline outline-1 hover:bg-green-700">
          <BsPlus color="white" size={28} />
          Add User
        </button>
        <CardBody className="overflow-x-scroll px-0 pt-0 pb-2">
          <table className="w-full min-w-[640px] table-auto">
            <thead>
              <tr>
                {[
                  "full name",
                  "role",
                  "status",
                  "created at",
                  "updated at",
                  "action",
                ].map((el) => (
                  <th
                    key={el}
                    className="border-b border-blue-gray-50 py-3 px-5 text-left"
                  >
                    <Typography
                      variant="small"
                      className="text-[11px] font-bold uppercase text-blue-gray-400"
                    >
                      {el}
                    </Typography>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {userlist.map(
                (data, key) => {
                  return (
                    <tr key={key}>
                      <td className='p-5'>
                        <div className="flex items-center gap-4">
                          <Avatar
                            src="https://cdn-icons-png.flaticon.com/512/149/149071.png"
                            alt="/"
                            size="sm"
                          />
                          <div>
                            <Typography
                              variant="small"
                              color="blue-gray"
                              className="font-semibold"
                            >
                              {data.fullname}
                            </Typography>
                            <Typography className="text-xs font-normal text-blue-gray-500">
                              {data.email}
                            </Typography>
                          </div>
                        </div>
                      </td>
                      <td className='p-5'>
                        <Typography className="text-xs font-semibold text-blue-gray-600">
                          {data.role}
                        </Typography>
                      </td>
                      <td className='p-5'>
                        <button className="text-xs font-semibold p-2 rounded-xl" style={{backgroundColor: data.active === "true"? "#90EE90":"#ff726f"}} onClick={() => updateUser(data._id, data.active)}>{data.active === 'true'? 'Active': 'Inactive'}</button>
                      </td>
                      <td className='p-5'>
                        <Typography className="text-xs font-semibold text-blue-gray-600">
                          {moment(data.createdAt).format("Do MMMM YYYY")}
                        </Typography>
                      </td>
                      <td className='p-5'>
                        <Typography className="text-xs font-semibold text-blue-gray-600">
                          {moment(data.updatedAt).format("Do MMMM YYYY")}
                        </Typography>
                      </td>
                      <td className='p-5'>
                        <Typography className="text-xs font-semibold text-blue-gray-600">
                          <span
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              gap: "5px",
                            }}
                          >
                            <CgTrash
                              onClick={() => deleteUser(data._id, data.fullname)}
                              size={30}
                              color="red"
                              className="hover:scale-110"
                            />
                          </span>
                        </Typography>
                      </td>
                    </tr>
                  );
                }
              )}
            </tbody>
          </table>
        </CardBody>
      </Card>
      <div>
        <ModalNewUser open={openModal} onClose={() => setOpenModal(false)} />
      </div>
    </div>
  );
}

export default Tables;
