import {
  UserPlusIcon,
  UserIcon,
  UserGroupIcon,
  ChartBarIcon,
} from "@heroicons/react/24/solid";

export const statisticsCardsData = [
  {
    color: "blue",
    icon: UserGroupIcon,
    title: "Active Users",
    value: "7",
    footer: {
      color: "",
      value: "",
      label: "",
    },
  },
  {
    color: "pink",
    icon: UserIcon,
    title: "Today's Users",
    value: "6",
    footer: {
      color: "",
      value: "",
      label: "",
    },
  },
  {
    color: "green",
    icon: UserPlusIcon,
    title: "New Users",
    value: "5",
    footer: {
      color: "",
      value: "",
      label: "",
    },
  },
  // {
  //   color: "orange",
  //   icon: ChartBarIcon,
  //   title: "Sales",
  //   value: "$103,430",
  //   footer: {
  //     color: "text-green-500",
  //     value: "+5%",
  //     label: "than yesterday",
  //   },
  // },
];

export default statisticsCardsData;
