export const authorsTableData = [
  {
    img: "/img/team-2.jpeg",
    name: "John Michael",
    email: "john@creative-tim.com",
    active: true,
    created: "23/04/18",
    lastloggedin: "15/5/22",

  },
  {
    img: "/img/team-1.jpeg",
    name: "Alexa Liras",
    email: "alexa@creative-tim.com",
    active: false,
    created: "11/01/19",
    lastloggedin: "15/5/22",
  },
  {
    img: "/img/team-4.jpeg",
    name: "Laurent Perrier",
    email: "laurent@creative-tim.com",
    active: true,
    created: "19/09/17",
    lastloggedin: "15/5/22",
  },
  {
    img: "/img/team-3.jpeg",
    name: "Michael Levi",
    email: "michael@creative-tim.com",
    active: true,
    created: "24/12/08",
    lastloggedin: "15/5/22",
  },
  {
    img: "/img/bruce-mars.jpeg",
    name: "Bruce Mars",
    email: "bruce@creative-tim.com",
    active: false,
    created: "04/10/21",
    lastloggedin: "15/5/22",
  },
  {
    img: "/img/team-2.jpeg",
    name: "Alexander",
    email: "alexander@creative-tim.com",
    active: false,
    created: "14/09/20",
    lastloggedin: "15/5/22",
  },
];

export default authorsTableData;
